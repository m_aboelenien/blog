class HashtagsController < ApplicationController
  def show
    @hashtag = Hashtag.find(params[:id])
    @hashtags = Hashtag.where(name: @hashtag.name)
  end

  def create
    @hashtag = Hashtag.new(hash_params)
    @hashtag.post = Post.find(params[:post_id])
    @hashtag.save
    redirect_to :back
  end

  private
  def hash_params
    params.require(:hashtag).permit(:name)
  end
end
