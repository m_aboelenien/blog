class User::PostsController < ApplicationController
  before_action :get_post, only: [:destroy,:show,:edit,:update]
  def index

  end
  def create
    @user = current_user
    @post = @user.posts.create(params.require(:post).permit(:title, :body))
    redirect_to user_post_path(@post)
  end
  def show
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end
  def edit
  end
  def update
    respond_to do |format|
      if(@post.update(post_params))
        format.html{redirect_to user_post_path,notice: 'Post updated successfully'}
      end
    end
  end



  private
  def get_post
    @post = Post.find(params[:id]) rescue nil
  end
  def post_params
    params.require(:post).permit(:title, :body)
  end

end
