class UsersController < ApplicationController
protect_from_forgery
before_action :logged_in_users, only: [:edit, :update]
  def index
    @user = current_user
  end
  def create
    @user  = User.new(user_params)
    if @user.save
      log_in @user
      redirect_to posts_path
    end
  end
  def show
    @user = current_user
    @posts = Post.all
  end
  def new
  end
  def edit
    @user = current_user
  end

  def update
    if(current_user.update_attributes(user_params))
      flash[:success] = "Profile updated"
      redirect_to posts_path
    end
  end

  private
  def user_params
      params.require(:user).permit(:username, :email, :password, :image ,:remove_image)
  end
  def logged_in_users
      unless logged_in? && current_user == User.find(params[:id])
          flash[:danger] = "Please log in."
          log_out
          redirect_to login_url
      end
  end
end
