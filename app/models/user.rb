class User < ActiveRecord::Base
  validate :image_size_validation
  has_many :posts
  has_many :comments
  has_secure_password
  mount_uploader :image, ImageUploader

  private
  def image_size_validation
    errors[:image] << "should be less than 500KB" if image.size > 0.5.megabytes
  end
end
