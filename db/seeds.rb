# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
User.destroy_all
Post.destroy_all
Comment.destroy_all
Hashtag.destroy_all

@user1 = User.create(username: "mohamed",email: "mohamed@gmail.com",password:"password")
@user2 = User.create(username:  "ahmed",email: "ahmed@gmail.com",password:"password")
@user3 = User.create(username:"mostafa",email: "mostafa@gmail.com",password:"password")
@user4 = User.create(username:"rana",email: "rana@gmail.com",password:"password")
@user5 = User.create(username:"menna",email: "menna@gmail.com",password:"password")
@user6 = User.create(username:"mariam",email: "mariam@gmail.com",password:"password")

@post1 = Post.create(title:"hello",body:"Nooooo",user: @user1)
@post2 = Post.create(title:"hello1",body:"Yes",user:@user1)
@post3 = Post.create(title:"hello2",body:"Nooooo",user:@user2)
@post4 = Post.create(title:"hello3",body:"Yes",user:@user3)
@post5 = Post.create(title:"hello4",body:"Yes",user:@user4)
@post6 = Post.create(title:"hello5",body:"Nooooo",user:@user1)
@post7 = Post.create(title:"hello5",body:"Yes",user:@user5)
@post8 = Post.create(title:"hello6",body:"Nooooo",user:@user1)
@post9 = Post.create(title:"hello7",body:"Yes",user:@user3)
@post10 = Post.create(title:"hell8",body:"Yes",user:@user6)
@post11 = Post.create(title:"hell8",body:"Nooooo",user:@user1)
@post12 = Post.create(title:"hello9",body:"Yes",user:@user1)
@post13 = Post.create(title:"hello10",body:"Nooooo",user:@user2)
@post14 = Post.create(title:"hellodas",body:"Yes",user:@user1)

@comment1 = Comment.create(body:"niceee",user:@user1,post:@post1)
@comment2 = Comment.create(body:"holla",user: @user2,post:@post2)
@comment3 = Comment.create(body:"ohhhh",user: @user3,post:@post3)
@comment4 = Comment.create(body:"nic",user: @user1,post:@post4)
@comment5 = Comment.create(body:"ohhhh",user: @user2,post:@post7)
@comment6 = Comment.create(body:"ohhhh",user: @user4,post:@post11)
@comment7 = Comment.create(body:"niceee",user: @user5,post:@post1)
@comment8 = Comment.create(body:"niceee",user:@user6,post:@post1)
@comment9 = Comment.create(body:"badd",user:@user5,post:@post2)
@comment10 = Comment.create(body:"love it!!",user: @user6,post:@post2)
@comment11 = Comment.create(body:"niceee",user: @user1,post:@post3)
@comment12 = Comment.create(body:"niceee",user: @user1,post:@post1)

@Hashtag1 = Hashtag.create(name:"cars",post:@post1)
@Hashtag2 = Hashtag.create(name:"cars",post:@post2)
@Hashtag3 = Hashtag.create(name:"cars",post:@post3)
@Hashtag4 = Hashtag.create(name:"cars",post:@post4)
@Hashtag5 = Hashtag.create(name:"red",post:@post1)
@Hashtag6 = Hashtag.create(name:"red",post:@post2)
@Hashtag7 = Hashtag.create(name:"red",post:@post3)
@Hashtag8 = Hashtag.create(name:"red",post:@post4)
@Hashtag9 = Hashtag.create(name:"red",post:@post5)
@Hashtag10 = Hashtag.create(name:"red",post:@post6)
